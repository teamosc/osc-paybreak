<?php

namespace OSC\Ecommerce\Paybreak\Http\Actions;

use App\Application\Events\DispatchesEvents;
use OSC\Ecommerce\Paybreak\Http\Requests\PaybreakWebhookRequest;
use Illuminate\Database\DatabaseManager;
use Illuminate\Foundation\Bus\DispatchesJobs;

class HandlePaybreakWebhook
{
    use DispatchesJobs;
    use DispatchesEvents;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    private $generateLeadsFor = [
        //'REFERRED',
        //'PREDECLINE',
        'DECLINED',
    ];

    private $statuses = [
        'DRAFT'                 => 1,
        'PROPOSAL'              => 1,
        'INFO-NEEDED'           => 2,
        'REFERRED'              => 3,
        'DECLINED'              => 4,
        'ACTION-CUSTOMER'       => 5,
        'REFER'                 => 5,
        'ACTION-RETAILER'       => 6,
        'LEAD GENERATED'        => 6,
        'ACTION-LENDER'         => 7,
        'ACCEPTED'              => 7,
        'DEPOSIT-PAID'          => 8,
        'SIGNED'                => 8,
        'READY'                 => 8,
        'VERIFIED'              => 8,
        'AWAITING-ACTIVATION'   => 8,
        'AWAITING-CANCELLATION' => 9,
        'PARTIALLY-ACTIVATED'   => 10,
        'ACTIVATED'             => 11,
        'COMPLETED'             => 12,
        'CANCELLED'             => 13,
        'REFUNDED'              => 14,
    ];

    private $markAsCompleteFor = [
        //'DEPOSIT-PAID',
        'SIGNED',
        'READY',
        'COMPLETED',
    ];

    public function __construct(DatabaseManager $databaseManager)
    {
        $this->databaseManager = $databaseManager;
    }

    public function __invoke(PaybreakWebhookRequest $request)
    {

        \Mail::raw(serialize($request->all()), function($message) {
            $message->subject('Paybreak webhook');
            $message->to('steven.nash@openstudycollege.com');
        });


        if ($order = $this->getOrder($request)) {

            $this->updateOrder($request, $order);

            return response('OK', 200);
            return 'OK';
        }

        return response('Could not find corresponding order for Paybreak application', 200);
    }


    private function getOrder(PaybreakWebhookRequest $request)
    {
        if ($order = $this->getOrderByMetadata($request)) {
            return $order;
        }

        if ($order = $this->getOrderByApplication($request)) {
            return $order;
        }

        if ($order = $this->getOrderByProposal($request)) {
            return $order;
        }

        if ($order = $this->getOrderByReference($request)) {
            return $order;
        }
    }



    private function updateOrder(PaybreakWebhookRequest $request, $order)
    {
        if ($this->noChangeRequired($request, $order)) {
            return;
        }

        $order->pay_4_later_status = $request->get('status');
        //$order->pay_4_later_ref    = $request->get('application');

        if ($this->requiresCompletion($request)) {
            $order->order_status = 'Complete';
            $order->TxAuthNo     = 1;
        }

        if ($request->get('status') == 'DEPOSIT-PAID') {
            $order->order_date = date('Y-m-d');
        }

        $order->save();

        if (in_array($request->get('status'), $this->generateLeadsFor)) {
            if ($failedBasket = $this->getFailedBasket($order->orderID)) {
                $this->dispatchEvent('failed p4l', $failedBasket);
            }
            $this->createLead($order);
        }
    }


    private function noChangeRequired(PaybreakWebhookRequest $request, $order): bool
    {
        if ($this->statusesAreIdentical($request, $order)) {
            return true;
        }

        if ($this->newStatusIsObsolete($request, $order)) {
            return true;
        }

        return false;
    }

    private function requiresCompletion(PaybreakWebhookRequest $request): bool
    {
        return in_array($request->get('status'), $this->markAsCompleteFor);
    }

    private function getOrderByMetadata($request)
    {
        if ($request->filled('metadata')) {
            $metadata = $request->get('metadata');

            if (array_key_exists('Order Number', $metadata)) {
                $order =  \App\Domain\Ecommerce\Entities\Order::where([
                        'pay_4_later_ref' => $metadata['Order Number'],
                        'payplan'         => 'Paybreak',
                    ]
                )
                ->whereNotNull('pay_4_later_ref')
                ->first();

                return $order;
            }
        }
    }

    private function getOrderByProposal($request)
    {
        return \App\Domain\Ecommerce\Entities\Order::where([
                'pay_4_later_ref' => $request->get('proposal'),
                'payplan'         => 'Paybreak',
            ]
        )
            ->whereNotNull('pay_4_later_ref')
            ->first();
    }

    private function getOrderByApplication($request)
    {
        return \App\Domain\Ecommerce\Entities\Order::where([
                'orderID' => $request->get('application'),
                'payplan' => 'Paybreak',
            ]
        )->first();
    }

    private function getOrderByReference($request)
    {
        return \App\Domain\Ecommerce\Entities\Order::where([
                'orderID' => $request->get('reference'),
                'payplan' => 'Paybreak',
            ]
        )->first();
    }

    private function statusesAreIdentical(PaybreakWebhookRequest $request, $order): bool
    {
        return $order->pay_4_later_status == $request->get('status');
    }

    private function newStatusIsObsolete(PaybreakWebhookRequest $request, $order): bool
    {
        if (isset($order->pay_4_later_status)) {
            if ($this->statuses[$order->pay_4_later_status] >= 10) {
                return false;
            }
            return $this->statuses[$order->pay_4_later_status] > $this->statuses[$request->get('status')];
        }

        return false;
    }

    private function createLead(Order $order)
    {
        $student = $order->student()->first();
        $item    = $order->orderItems()->first();

        $leadData = [
            'name'         => $student->student_fullname,
            'email'        => $student->student_email,
            'telephone'    => $student->student_phone,
            'enquiry'      => 'Order ID: '.$order->orderID.'. Payment ref: '.$order->pay_4_later_ref,
            'course'       => $item->name,
            'source'       => strtoupper($order->pay_4_later_status). ' Paybreak',

        ];

        $this->dispatch(new SaveLead($leadData));

        if ($failedBasket = $this->getFailedBasket($order->orderID)) {
            //$this->dispatchEvent('failed p4l', $failedBasket);
        }

        $order->pay_4_later_status = 'LEAD GENERATED';
        $order->save();
    }

    private function getFailedBasket($orderId)
    {
        return $this->databaseManager
            ->connection('intranet')
            ->table('orders')
            ->join('students', 'students.studentID', '=', 'orders.studentID')
            ->join('orders_items', 'orders_items.orderID', '=', 'orders.orderID')
            ->where('orders.orderID', '=', $orderId)
            ->limit(1)
            ->first();
    }
}