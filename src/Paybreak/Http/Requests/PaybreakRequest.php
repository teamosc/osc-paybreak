<?php

namespace OSC\Ecommerce\Paybreak\Http\Requests;

use App\Application\Request;

class PaybreakRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @param $factory
     * @return mixed
     */
    public function rules()
    {
        $time18 = strtotime("-18 year", time());
        $time75 = strtotime("-75 year", time());
        return [
            'student_title'              => 'required',
            'student_firstname'          => 'required',
            'student_lastname'           => 'required',
            'student_email'              => 'required|email',
            'student_phone'              => 'required',
            'student_address1'           => 'required',
            'student_address2'           => 'required',
            //'student_country'            => 'required',
            'student_postcode'           => 'required',
            'customer_dob_day'           => 'required',
            'customer_dob_month'         => 'required',
            'customer_dob_year'          => 'required|integer|max:'.date('Y',$time18) . '|min:' . date('Y',$time75) ,
            'delivery_firstname'         => 'required_without:delivery',
            'delivery_lastname'          => 'required_without:delivery',
            'delivery_address1'          => 'required_without:delivery',
            'finance-term'               => 'required',
            'finance-deposit'            => 'required',
        ];
    }
}
