<?php

namespace OSC\Ecommerce\Paybreak\Http\Requests;

use App\Exceptions\LogsExceptions;
use App\Application\Request;

class PaybreakWebhookRequest extends Request {

    use LogsExceptions;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {   \Log::info($this->all());
        return [
            'application'                   => 'required',
            'proposal'                      => 'required',
            //'reference'                           => 'required',
            'status'                        => 'required',
            //'Identification.InstallationID' => 'required',
            //'Identification.RetailerUniqueRef' => 'required',
        ];
    }
}