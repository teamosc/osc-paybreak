<?php

namespace OSC\Ecommerce\Paybreak;

class PaybreakFinanceDeal implements \OSC\Ecommerce\FinanceDeal {

    private $financeProduct;

    private $depositAmount;

    private $depositPercentage;

    private $duration;

    private $interestRate;

    private $regularPayment;

    private $cashPrice;

    private $loanAmount;

    private $loanRepayment;

    private $totalRepayable;

    private $costOfLoan;

    public function __construct($response, $financeProduct)
    {
        \Log::info($financeProduct);
        \Log::info($response);
        $this->financeProduct    = $financeProduct;
        $this->cashPrice         = $response['purchase_price'];
        $this->depositAmount     = $response['deposit_amount'];
        $this->depositPercentage = ($this->depositAmount / $this->cashPrice) * 100;
        $this->duration          = $response['agreement_duration'];
        $this->interestRate      = $response['interest_rate'];
        $this->regularPayment    = $response['monthly_payment_amount'];
        $this->loanAmount        = $response['credit_amount'];
        $this->loanRepayment     = $this->regularPayment * $this->duration;
        $this->totalRepayable    = $response['total_repayable_amount'];
        $this->costOfLoan        = $this->totalRepayable - $this->cashPrice;
    }

    public function expose()
    {
        return get_object_vars($this);
    }

    public function getFinanceProduct()
    {
        return $this->financeProduct;
    }

    public function getDepositAmount()
    {
        return $this->depositAmount;
    }

    public function getDepositPercentage()
    {
        return $this->depositPercentage;
    }

    public function getDuration()
    {
        return $this->duration;
    }

    public function getInterestRate()
    {
        return $this->interestRate;
    }

    public function getRegularPayment()
    {
        return $this->regularPayment;
    }

    public function getCashPrice()
    {
        return $this->cashPrice;
    }

    public function getLoanAmount()
    {
        return $this->loanAmount;
    }

    public function getTotalRepayable()
    {
        return $this->totalRepayable;
    }

    public function getCostOfLoan()
    {
        return $this->getCostOfLoan();
    }

    public function getLoanRepayment()
    {
        return $this->getLoanRepayment();
    }
}