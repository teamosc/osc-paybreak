<?php

namespace OSC\Ecommerce\Paybreak;

use Illuminate\Database\DatabaseManager;
use OSC\Ecommerce\Calculator;
use App\Services\Ecommerce\Calculators\BaseCalculator;

/**
 *  Calculates item prices based on their discount groups
 */
class PaybreakCalculator extends BaseCalculator implements Calculator {

    /**
     * @var GetFinanceDetailsForProduct
     */
    private $client;

    public function __construct(\PayBreak\Sdk\ApiClient\ProviderApiClient $client, DatabaseManager $databaseManager, $discountGroupId = null)
    {
        $this->client          = $client;
        $this->discountGroupId = $discountGroupId ?? null;
        $this->databaseManager = $databaseManager;
    }

    public function cheapest($item)
    {
        return $this->lowestNonZeroValueOf([
            $this->calculateItemPrice($item, 'online') > env('FINANCE_FEE')
                ? $this->calculateItemPrice($item, 'online')
                : 0,
            $this->calculateItemPrice($item, 'course-pack') > env('FINANCE_FEE')
                ? $this->calculateItemPrice($item, 'course-pack')
                : 0,
        ]);
    }

    public function calculateCheapestRegularPayment($item, $studyMethod = null)
    {
        return $this->calculateRegularPaymentForItem(
            $item,
            $this->getCheapestStudyMethodForItem($item),
            $this->getDefaultFinanceTerm(),
            //config('osc-ecommerce.finance.default-finance-term'),
            config('osc-ecommerce.finance.default-deposit')
        );
    }

    public function calculateFinancePerMonthRegularPayment($total)
    {
        $percentage = ($total/100)*10;
        $total = $total - $percentage;
        $interest = ($total/100)*16.9;
        $total = $total + $interest;
        $perMonth = $total/24;
        return $perMonth;
    }

    private function getDefaultFinanceTerm()
    {
        $term = config('osc-ecommerce.finance.default-finance-term');
        return config(sprintf('osc-ecommerce.finance.Paybreak.finance-products.%s.name', $term));
    }

    public function calculateRegularPaymentForItem($item, $studyMethod, $product, $deposit)
    {
        try {
            //\Log::info($this->calculateItemPrice($item, $studyMethod));
            return $this->calculateRegularPayment(
                $this->calculateItemPrice($item, $studyMethod),
                $product,
                $deposit
            );
        } catch (\Exception $e) {
            \Log::info('It didnt work');
            \Log::info($e->getMessage());
            return 0;
        }
    }

    public function calculateRegularPayment($amount, $product, $deposit)
    {
        return $this->getPaymentDetails($amount, $product, $deposit)->getRegularPayment();
    }

    public function getPaymentDetails($amount, $finance, $deposit): \OSC\Ecommerce\FinanceDeal
    {
        try {
            $deposit = $amount * (0.01 * $deposit);
            $country = 'GB';
            \Paybreak::setMerchant(env('DIVIDO_API_KEY'));
            $response = \Paybreak_DealCalculator::all(compact(
                'finance',
                'amount',
                'deposit',
                'country'
            ));

            return $this->transformResponse($response, compact('finance'));
        } catch (\Exception $e) {
            \Log::info($e);
            dd($e->getMessage());
        }
    }

    private function transformResponse($response, $financeProduct): \OSC\Ecommerce\FinanceDeal
    {
        return new \OSC\Ecommerce\Paybreak\PaybreakFinanceDeal($response, $financeProduct);
    }

    protected function calculateCoursePrice($item, $studyMethod)
    {
        if ($this->itemInDiscount($item)) {
            return ($this->studyingOnline($studyMethod)
                    ? $item->online_price
                    : $item->retail_price) + env('FINANCE_FEE');
        }

        return ($this->studyingOnline($studyMethod)
                ? $item->fullprice_online
                : $item->fullprice) + env('FINANCE_FEE');
    }

    protected function calculatePackagePrice($item, $studyMethod)
    {
        if ($this->itemInDiscount($item)) {
            return ($this->studyingOnline($studyMethod)
                    ? $item->online_price
                    : $item->retail_price) + env('FINANCE_FEE');
        }

        return $this->studyingOnline($studyMethod)
            ? $item->fullprice_online + env('FINANCE_FEE')
            : $item->fullprice + env('FINANCE_FEE');
    }

    private function getCheapestStudyMethodForItem($item)
    {
        $onlinePrice     = $this->calculateItemPrice($item, 'online');
        $coursePackPrice = $this->calculateItemPrice($item, 'course-pack');

        if (($onlinePrice < $coursePackPrice) && $onlinePrice > env('FINANCE_FEE')) {
            return 'online';
        }

        return 'course-pack';
    }
}