<?php

namespace OSC\Ecommerce\Paybreak\Forms;

use App\Domain\Ecommerce\Jobs\CreateOrder;
use OSC\Ecommerce\Paybreak\Http\Requests\PaybreakRequest;
use OSC\Contracts\Multisite\SiteResolver;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Infrastructure\Ecommerce\Repositories\CheckoutRepository;
use OSC\Ecommerce\Basket;

class PaybreakPaymentFormHandler implements \OSC\Contracts\Forms\FormHandler
{
    use DispatchesJobs;
    /**
     * @var \Illuminate\Http\Request
     */
    private $request;
    /**
     * @var CreateOrder
     */
    private $createOrder;
    /**
     * @var SiteResolver
     */
    private $siteResolver;

    public function __construct(SiteResolver $siteResolver, PaybreakRequest $request, CreateOrder $createOrder, CheckoutRepository $checkoutRepository)
    {
        $this->siteResolver = $siteResolver;
        $this->request      = $request;
        $this->createOrder  = $createOrder;
        $this->checkoutRepository = $checkoutRepository;
    }

    public function persist()
    {
        $student     = $this->createStudent();
        $this->order = $this->createOrder($student);
        $basket      = $this->getBasket()->contents();
        \Log::info($this->order);
        $basketProducts = $this->transformBasket($basket);
        \Paybreak::setMerchant($this->getApiKey());
        $this->response = \Paybreak_CreditRequest::create(
            [
                'deposit'                      => number_format(number_format((($this->order->Amount / 100) * $this->request->get('finance-deposit')), 2, '.', ''), 0, '.', ''),
                'finance'                      => $this->request->get('finance-term'),
                'country'                      => 'GB',
                'language'                     => 'EN',
                'currency'                     => 'GBP',
                'customer[title]'              => $student->student_title,
                'customer[first_name]'         => $student->student_firstname,
                'customer[last_name]'          => $student->student_lastname,
                'customer[country]'            => 'GB',
                'customer[address][text]'      => $student->student_address1.' '.$student->student_address2.' '.$student->student_postcode,
                'customer[address][postcode]'  => $student->student_postcode,
                'customer[address][street]'    => $student->student_address1,
                'customer[email]'              => $student->student_email,
                'customer[phoneNumber]'        => $student->student_phone,
                'customer[dateOfBirthYear]'    => \Carbon\Carbon::createFromFormat('Y-m-d', $student->DoB )->format('Y'),
                'customer[dateOfBirthMonth]'   => \Carbon\Carbon::createFromFormat('Y-m-d', $student->DoB )->format('m'),
                'customer[dateOfBirthDay]'     => \Carbon\Carbon::createFromFormat('Y-m-d', $student->DoB )->format('d'),
                //'customer[mobile_number]'  => '+441211231234',
                'metadata[Invoice Number]'     => time(),
                'metadata[Order Number]'       => $this->order->order_ref,
                'amount'                       => number_format($this->order->Amount, 2, '.', ''),
                'responseUrl'                  => env('DIVIDO_RESPONSE_URL'),
                'checkoutUrl'                  => 'http://'.$this->request->getHttpHost().'/basket/retrieve/'.$this->order->orderID.'/'.$this->order->studentID,
                $basketProducts
            ],
            \Config::get('osc-ecommerce.finance.Paybreak.api-key')
        );

        /*
         * responseUrl - The URL where we send notification about the payment (Optional, String)
            Example `http://www.webshop.com/response.php`
            checkoutUrl - A URL which Paybreak redirects the customer to if they get declined or wish to cancel their application (Optional, String)
            Example `http://www.webshop.com/checkout`
            redirectUrl - The URL the customer will get redirected to after a successful application (Optional, String)
         */

        return;
    }

    public function response($request, $data = null)
    {
        if ($this->response->status == 'ok') {
            $this->order->pay_4_later_ref = $this->order->order_ref;
            $this->order->save();
            \Log::info('DIVIDO RESPONSE: '.$this->response);
            return redirect($this->response->url);
        }

        return redirect($this->response);
    }

    protected function createStudent()
    {
        return $this->dispatch(new \App\Domain\Academic\Jobs\CreateStudent($this->transformStudent()));
    }

    private function transformStudent()
    {
        $input        = $this->request->all();
        $input['DoB'] = $this->transformDateOfBirth($input);
        //$input        = $this->appendCompanyNames($input);

        return $input;
    }

    private function transformDateOfBirth($input) {
        return sprintf(
            '%s-%s-%s',
            $input['customer_dob_year'],
            $input['customer_dob_month'],
            $input['customer_dob_day']
        );
    }

    private function transformBasket($basket)
    {
        $products = array();
        $i = 1;
        foreach ($basket as $product){
            $products['products'][$i]['price'] = $product['price'];
            $products['products'][$i]['name'] = $product['title'];
            $products['products'][$i]['quantity'] = 1;
            $i++;
        }
        return $products;
    }

    /**
     * @param $student
     * @return mixed
     */
    private function createOrder($student)
    {
        return $this->dispatch($this->createOrder->withStudent($student));
    }

    private function getApiKey()
    {
        return \Config::get(
            sprintf(
                'osc-ecommerce.finance.Paybreak.api-keys.%s',
                $this->siteResolver->resolve()->getKey()
            )
        );
    }

    /**
     * @return Basket
     */
    private function getBasket(): Basket
    {
        return $this->checkoutRepository->getBasket();
    }
}
